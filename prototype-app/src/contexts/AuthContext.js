import React, { useContext, useState, useEffect } from "react";
import { auth } from "../firebase";

const AuthContext = React.createContext()

export function useAuth(){
    return useContext(AuthContext)
}

export function AuthProvider({children}){

    const [currentUser, setCurrentUser] = useState()
    const [storedUserData, setStoredUserData] = useState()
    const [loading, setLoading] = useState(true)

    function login(email, password){
        return auth.signInWithEmailAndPassword(email, password)
    }

    function logout(){
        return auth.signOut()
    }

    function applyStoredUserData(userData){
        setStoredUserData(userData)
    }

    useEffect(() => {
        const unsubscribe = auth.onAuthStateChanged(user => {
          setCurrentUser(user)
        //   setLoading(false);
            setLoading(false);
        })
    
        return unsubscribe
      }, [])

    const value = {
        currentUser,
        login,
        logout,
        storedUserData,
        applyStoredUserData
    }

    return (
        <AuthContext.Provider value={value}>
            {!loading && children}
        </AuthContext.Provider>
    )

}