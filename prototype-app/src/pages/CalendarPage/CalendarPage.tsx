import React from 'react'
import { Calendar, momentLocalizer, Views } from 'react-big-calendar';
import moment from 'moment';
import CalendarHeader from './components/CalendarHeader';
require('react-big-calendar/lib/css/react-big-calendar.css');

const CalendarPage = () => {

    const localizer = momentLocalizer(moment);
    let myEventsList = [
        {
            id: 0,
            title: 'Software Projects Final Demo',
            allDay: true,
            start: new Date(2021, 3, 15),
            end: new Date(2021, 3, 15)
        },
        {
            id: 1,
            title: 'Object Oriented Quiz',
            allDay : true,
            start : new Date(2021, 3, 18),
            end: new Date(2021, 3, 18)
        },
        {
            id: 1,
            title: 'Assignment 8',
            allDay : true,
            start : new Date(2021, 3, 21),
            end: new Date(2021, 3, 21)
        },
        {
            id: 1,
            title: 'Quiz 4',
            allDay : true,
            start : new Date(2021, 3, 24),
            end: new Date(2021, 3, 24)
        }
    ]

    let allViews = Object.keys(Views).map(k => Views[k])

    const ColoredDateCellWrapper = ({ children }) =>
        React.cloneElement(React.Children.only(children), {
            style: {
            backgroundColor: 'lightblue',
            },
        })

    
    return(
        <div>
            <CalendarHeader/>
            <Calendar
                events={myEventsList}
                views={allViews}
                step={60}
                showMultiDayTimes
                defaultDate={new Date(2021, 3, 1)}
                components={{
                    timeSlotWrapper: ColoredDateCellWrapper,
                }}
                localizer={localizer}
                style={{ height: '80vh', width: '80vw' }}
            />  
        </div> 
    )
}

export default CalendarPage;