import React from 'react';
import {Grid} from '@material-ui/core'
import SwipeableSideBar from '../../../components/SwipeableSideBar/SwipeableSideBar'
import CourseSelect from '../../../components/CourseSelect/CourseSelect';
const CalendarHeader = () => {

    return(

        <div style={{width: '100vw', overflowX: 'hidden'}}>

            <Grid container direction="row" justify="flex-start" alignItems="center" spacing={4}>
                <Grid item ><SwipeableSideBar></SwipeableSideBar></Grid>
                <Grid item>
                    <h2>Calendar</h2>
                </Grid>
                <Grid item></Grid>
                <Grid item></Grid>
                <Grid item></Grid>
                <Grid item><CourseSelect/></Grid>
            </Grid>

        </div>

    )

}

export default CalendarHeader;