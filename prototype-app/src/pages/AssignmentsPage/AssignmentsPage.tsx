import React from 'react'
import {Grid, Button, Typography, Paper, Table, TableHead, TableBody, TableContainer, TableRow, TableCell} from '@material-ui/core';
import CreateAssignmentModal from './components/CreateAssignmentModal';
import {useParams} from 'react-router-dom';
import {useAuth} from '../../contexts/AuthContext';
import {Class} from '../../models/class.model';
import {Assignment} from '../../models/assignment.model';
import apiWrapper from '../../utils/apiWrapper';
import AssignmentRow from './components/AssignmentRow';
import SwipeableSideBar from '../../components/SwipeableSideBar/SwipeableSideBar'
import firebase from 'firebase/app';

const AssignmentsPage = () => {

    const [showCreate, setShowCreate] = React.useState(false);
    const [teacherID, setTeacherID] = React.useState('');

    const [assignmentsLoaded, setAssignmentsLoaded] = React.useState(false);
    const [assignmentArray, setAssignmentArray] = React.useState<Array<Assignment>>([]);


    const {classID} = useParams(); 
    const { storedUserData } = useAuth();
    const classData : Array<Class> = storedUserData.classData;
    var classObj = classData.find((a)=>{return a.classID === classID});

    
    const createAssignmentPressed = () => {
        //show dialog
        const teacher = getTeacherID(classID, storedUserData.classData);
        setTeacherID(teacher);
        setShowCreate(true);
    }

    const closeModal = () => {
        setShowCreate(false);
    }

    React.useEffect(
        () => {

            if(!assignmentsLoaded){
                let tempArray : Array<Assignment> = [];
                apiWrapper.get(`getAssignments/${classID}`).then((res) => {

                    let tempAssignmentArray : Array<Assignment> = [];

                    res.data.forEach((assign) => {

                        let assignment = {
                            homeworkName : assign["homeworkName"],
                            description : assign["description"],
                            dueDateTime : new Date(assign["dueDateTime"]),
                            fileLink : assign["fileLink"]
                        };

                        tempAssignmentArray.push(assignment);

                    })

                    // settempAssignmentArray(tempAssignmentArray);
                    setAssignmentsLoaded(true);
                    setAssignmentArray(tempAssignmentArray);

                })
            }
            

        }, [setAssignmentsLoaded, setAssignmentArray, classID]
    )
    
    return (
        <div>
            <SwipeableSideBar/>
            <CreateAssignmentModal classID={classID} teacherID={teacherID} showModal={showCreate} closeModal={closeModal}/>
            <Grid container direction="column" justify="center" alignItems="center" style={{marginLeft: "1vw", marginTop: "1vw"}}>
                <Grid item>
                    <div>
                        <h3 style={{width: "100vw"}}>
                            Assignments for {classObj?.className}
                        
                            <Button variant="outlined" color="primary" onClick={createAssignmentPressed} style={{marginLeft: "25px"}}>
                                <Typography>
                                    Create Assignment
                                </Typography>
                            </Button>

                        </h3>
                    </div>
                </Grid>
                <Grid item>
                    <div style={{width : '90vw'}}>
                        <TableContainer component={Paper}>
                            <Table aria-label="simple table">
                                <TableHead>
                                <TableRow>
                                    <TableCell>
                                        <h3>
                                            Assignment
                                        </h3>
                                    </TableCell>
                                    <TableCell align="left">
                                        <h3>
                                            Attachment
                                        </h3>
                                    </TableCell>
                                    <TableCell align="left">
                                        <h3>
                                            Due Date
                                        </h3>
                                    </TableCell>
                                </TableRow>
                                </TableHead>
                                <TableBody>
                                    {/* <TableRow>
                                        <TableCell>Hello</TableCell>
                                    </TableRow> */}
                                    {
                                        (assignmentsLoaded) ? (
                                            assignmentArray.map((assignment, index) => (
                                                <AssignmentRow
                                                    key={index} 
                                                    title={assignment.homeworkName!} 
                                                    description={assignment.description!} 
                                                    dueDate={assignment.dueDateTime!} 
                                                    fileLink={"https://firebasestorage.googleapis.com/v0/b/prototypeapp-28f76.appspot.com/o/assignments%2F0aaf3306-5b9e-41fa-97e2-cea98848cfe6?alt=media&token=66ee06b5-62fa-4408-87b2-98eecce2427d"} 
                                                />
                                            ))
                                        ) : (undefined)
                                    }
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </div>
                </Grid>
            </Grid>
        </div>
    )

}

async function getDownloadURLs(assignments : Array<Assignment>){

    var storage = firebase.storage();


    let promises : Array<Promise<any>> = [];

    assignments.forEach((assignment) => {

        const path = `gs://prototypeapp-28f76.appspot.com/${assignment.fileLink!}`
        var refToImage = storage.refFromURL(path);
        let promise = refToImage.getDownloadURL();
        promises.push(promise);
    })

    return Promise.all(promises);   
}

function getTeacherID(classID : string, classList: Array<Class>){

    var teacher = "";
    for(let singleClass of classList){
        if(classID === singleClass.classID!){
            teacher = singleClass.teacherID!;
        }
    }
    return teacher;
}

export default AssignmentsPage;