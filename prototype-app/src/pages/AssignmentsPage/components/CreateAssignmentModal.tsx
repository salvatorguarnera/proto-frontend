import React from 'react'
import {Dialog, Grid, IconButton, TextField, Button, Typography, Snackbar} from '@material-ui/core'
import { Alert } from '@material-ui/lab';
import CloseIcon from '@material-ui/icons/Close';
import firebase from 'firebase/app'
import {uuid} from 'uuidv4';
import apiWrapper from '../../../utils/apiWrapper';

//https://material-ui.com/components/pickers/

const CreateAssignmentModal: React.FC<{classID : string, teacherID : string, showModal : boolean, closeModal: () => void;}> = ({classID, teacherID, showModal, closeModal}) => {

    const [selectedDate, setSelectedDate] = React.useState<Date>();
    const [selectedFile, setSelectedFile] = React.useState<File>();
    const [assignmentTitle, setAssignmentTitle] = React.useState('');
    const [assignmentDescription, setAssignmentDescription] = React.useState('');
    const [showSnackbar, setShowSnackbar] = React.useState(false);

    const handleDateChange = (event) => {
        const newDate = new Date(event.target.value);
        setSelectedDate(newDate);
      };

    const handleTitleChange = (event) => {
        setAssignmentTitle(event.target.value);
    }

    const handleDescriptionChange = (event) => {
        setAssignmentDescription(event.target.value);
    }

    const handleFileSelected = (event) => {
        const fileArray : Array<File> = Array.from(event.target.files);
        const selection = fileArray[0];
        setSelectedFile(selection);
    }

    async function createPressed(){

        const uploadMe : File = selectedFile!;
        const filePath = await uploadFile(uploadMe);

        const payload = {
            classID : classID,
            dueDateTime : selectedDate!.getTime(),
            homeworkName : assignmentTitle,
            description : assignmentDescription,
            teacherID : teacherID,
            fileLink : filePath
        };

        await createAssignment(payload);

        closeModal();
        setShowSnackbar(true);
    }

    const handleDialogClose = () => {
        closeModal();
    }

    const handleClose = (event: any, reason: any) => {
		if (reason === 'clickaway') {
			return;
		}
		setShowSnackbar(false);
	};

    return(
        <div>
                <Dialog open={showModal} fullWidth={true} maxWidth={'md'}>
                <Grid container direction="row" justify="space-between" alignItems="center">
                    <Grid item>
                        <h2 style={{paddingLeft: '5vw'}}>Create a new Assignment</h2>
                    </Grid>
                    <Grid item>
                        <div style={{paddingLeft: '5vw'}}>
                            <IconButton style={{paddingRight: '5vw'}} aria-label="close" onClick={handleDialogClose}>
                                <CloseIcon />
                            </IconButton>
                        </div>
                    </Grid>
                </Grid>
                <Grid container direction="column" justify="center" alignItems="center">

                    <Grid item>
                        <Grid container direction="row" justify="space-between" alignItems="center">
                            <Grid item>
                                <div style={{marginRight: '5vw'}}>
                                    <h4>Enter an Assignment Title: </h4>
                                </div>
                            </Grid>
                            <Grid item>
                                <div style={{marginLeft: '5vw'}}>
                                    <TextField placeholder={'Enter title here'} onChange={handleTitleChange}/>
                                </div>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item>
                        <Grid container direction="row" justify="space-between" alignItems="center">
                            <Grid item>
                                <div style={{marginRight: '5vw'}}>
                                    <h4>Enter a due date:</h4>
                                </div>
                            </Grid>
                            <Grid item>
                                <div style={{marginLeft: '5vw'}}>
                                    <TextField
                                        id="date"
                                        type="datetime-local"
                                        InputLabelProps={{
                                        shrink: true,
                                        }}
                                        onChange={handleDateChange}
                                    />
                                </div>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item>
                        <Grid container direction="row" justify="space-between" alignItems="center">
                            <Grid item>
                                <div style={{marginRight: '5vw'}}>
                                    <h4>Attach File</h4>
                                </div>
                            </Grid>
                            <Grid item>
                                <div style={{marginLeft: '5vw'}}>
                                    <label>
                                        <input onChange={handleFileSelected} type="file" accept="application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,image/jpeg,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"/>
                                    </label>
                                </div>
                            </Grid>
                        </Grid>
                    </Grid>
                    <Grid item>
                        <div style={{marginTop: '5vh', marginBottom: '5vh'}}>
                            <TextField
                                id="outlined-multiline-static"
                                label="Description"
                                multiline
                                rows={4}
                                variant="outlined"
                                style={{width: '50vw'}}
                                onChange={handleDescriptionChange}
                            />
                        </div>
                    </Grid>
                    <Grid item>
                        <div style={{marginBottom: '5vh'}}>
                            <Button color="secondary" variant="contained" onClick={createPressed}>
                                <Typography>Create Assignment</Typography>
                            </Button>
                        </div>
                    </Grid>
                </Grid>
            </Dialog>
            <Snackbar open={showSnackbar} autoHideDuration={6000} onClose={handleClose}>
				<Alert severity="success">Assignment Created!</Alert>
			</Snackbar>
        </div>

    )

}

async function createAssignment(payload){
    apiWrapper.post('createAssignment', payload).then(() => {
        return;
    })

}

async function uploadFile(file : File){

    const storageRef = firebase.storage().ref();
    const fileID = uuid();
    const refToFile = storageRef.child(`/assignments/${fileID}`)
    const pathway = refToFile.fullPath;
    await refToFile.put(file)
    return pathway;

}

export default CreateAssignmentModal;