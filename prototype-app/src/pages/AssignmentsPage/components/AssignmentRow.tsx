import React from 'react'
import {TableRow, TableCell, Button} from '@material-ui/core';
import moment from 'moment';

const AssignmentRow : React.FC<
{
    title : string,
    description : string,
    fileLink : string,
    dueDate : Date
}
> = ({title, description, fileLink, dueDate}) => {

    let dateString = moment(dueDate).format('LLL');

    return(
        <TableRow>
            <TableCell>
                {title}
                <br/>
                {description}
            </TableCell>
            <TableCell>
                <Button variant="outlined" color="secondary" href={fileLink}>
                    {`Download`}
                </Button>
            </TableCell>
            <TableCell>
                {dateString}
            </TableCell>
        </TableRow>
    )

}

export default AssignmentRow;