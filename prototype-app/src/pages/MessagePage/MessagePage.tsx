import React from 'react'
import MessageHeader from './components/MessageHeader';
import {Grid} from '@material-ui/core';
import SidePanel from './components/SidePanel';
import MessagePanel from './components/MessagePanel';
import CreateMessageModal from './components/CreateMessageModal';
import {ConversationDictionary, ConversationObject, MessageObject} from '../../models/message.model';
import {useAuth} from '../../contexts/AuthContext';
import apiWrapper from '../../utils/apiWrapper';

const MessagePage = () => {

    const [showCreateModal, setCreateModal] = React.useState(false);
    const { storedUserData } = useAuth();
    const [messagesLoaded, setMessagesLoaded] = React.useState(false);
    const [channelList, setChannelList] = React.useState<Array<string>>([]);

    React.useEffect(
        () => {

            if(!messagesLoaded){

                apiWrapper.get(`getChannels/${storedUserData.uid}`).then((res) => {

                    let channelArray : Array<string> = res.data;
                    setChannelList(channelArray);
                    setMessagesLoaded(true);

                })
                

            }

        }, [messagesLoaded, storedUserData]
    )

    
    const closeModal = () => {
        setCreateModal(false);
    }

    const showModal = () => {
        setCreateModal(true);
    }

    return (
        <div style={{width: '100vw'}}>
            <MessageHeader/>
            <div style={{paddingTop:'20px'}}></div>
            <Grid container direction="row" justify="flex-start" alignItems="flex-start">
                <Grid item>
                    <div style={{width: '15vw'}}>
                        <SidePanel showModal={showModal}/>
                    </div>    
                </Grid>   

                {
                    (messagesLoaded) ? (
                        <Grid item>
                            <div style={{width: '65vw'}}>
                                <MessagePanel channelList={channelList}/>
                            </div>
                        </Grid>
                    ) : (undefined)
                }

            </Grid>
            <CreateMessageModal isOpen={showCreateModal} closeModal={closeModal}/>
        </div>
    )
}



export default MessagePage;