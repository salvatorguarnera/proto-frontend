import React from 'react'
import {useLocation} from 'react-router-dom';
import {ConversationObject, MessageObject} from '../../../models/message.model';
import {Grid, TextField, Button, Typography} from '@material-ui/core'
import SwipeableSideBar from '../../../components/SwipeableSideBar/SwipeableSideBar'

const ConversationPage = () => {

    const [messageText, setMessageText] = React.useState('');
    const [conversationObject, setConversationObject] = React.useState<ConversationObject>({});
    const [messagesLoaded, setMessagesLoaded] = React.useState(false);
    let userID = "toID";
    let otherID = "fromID";

    let object : ConversationObject = {
        senderName: "Sal Guarnera",
        channelID: "channel",
        messages: [
            {
                body : "Hey do you know when homework 8 is due?  Also, are you converting to a PDF or a word document?",
                toID : "toID",
                fromID : "fromID",
                timestamp : {
                    _seconds : 100,
                    _milliseconds: 200,
                },
                read : false
            }
            ,{
                body : "I believe it is due the 19th.  At 11:59pm.  I am just gonna submit a PDF to be safe.",
                toID : "fromID",
                fromID : "toID",
                timestamp : {
                    _seconds : 100,
                    _milliseconds: 200,
                },
                read : false
            }
            ,
            {
                body : "Alright thanks man!",
                toID : "toID",
                fromID : "fromID",
                timestamp : {
                    _seconds : 100,
                    _milliseconds: 200,
                },
                read : false
            }
        ]
    }

    React.useEffect(() => {

        if(!messagesLoaded){
            setConversationObject(object);
            setMessagesLoaded(true);
        }

    }, [messagesLoaded, conversationObject])

    

    const textFieldChanged = (event) => {
        setMessageText(event.target.value);
    }

    const sendPressed = () => {
        
        let currentConvoObject : ConversationObject = conversationObject;

        let message = {
            body : messageText,
            toID : otherID,
            fromID : userID,
            timestamp : {
                _seconds : 100,
                _milliseconds : 200
            },
            read : false
        }

        currentConvoObject.messages!.push(message);
        setMessageText('');
        setConversationObject(currentConvoObject);

    }


    return (
        <div>
            <SwipeableSideBar/>
            <Grid container direction="column" justify="center" alignItems="center" style={{width : '100vw'}}>
                <Grid item>
                    <h3>Your conversation with {object.senderName}</h3>
                </Grid>
                {
                    (messagesLoaded) ? (
                        conversationObject.messages!.map((message, index) => (
                            MessageCell(message, userID)
                        ))
                    ) : (undefined)
                }
                <Grid item>
                    <br/>
                    <br/>
                    <Grid container direction="row" justify="flex-end" alignItems="center">
                        <Grid item>
                            <TextField value={messageText} placeholder="Type a message" style={{width: '75vw'}} onChange={textFieldChanged} />
                        </Grid>
                        <Grid item>
                            <Button onClick={sendPressed}>
                                <Typography>
                                    Send
                                </Typography>
                            </Button>
                        </Grid>
                    </Grid>                    
                </Grid>
            </Grid>
        </div>
    )

}

const MessageCell = (message : MessageObject, userID : string) => {

    const userIsSender = message.fromID! === userID
    return(
        <Grid item>
            <div style={{
                backgroundColor: userIsSender ? 'orange' : 'lightblue',
                margin: userIsSender ? '0 0 0 45vw' : '0 45vw 0 0',
                padding: '5px 5px 5px 5px',
                width: '15vw',
                height : 'auto'
                }} 
            >
                <p>
                    {message.body}
                </p>
            </div>
        </Grid>
    )

}

export default ConversationPage;