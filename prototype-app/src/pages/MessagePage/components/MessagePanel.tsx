import React from 'react';
import MessageCell from './MessageCell';
import {Grid} from '@material-ui/core'
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import RefreshIcon from '@material-ui/icons/Refresh';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import apiWrapper from '../../../utils/apiWrapper';
import {useAuth} from '../../../contexts/AuthContext';
import {ConversationDictionary, ConversationObject, MessageObject} from '../../../models/message.model';

const MessagePanel: React.FC<{channelList : Array<string>}> = ({channelList}) => {

    const [conversationLoaded, setConversationLoaded] = React.useState(false);
    const [conversationArray, setConversationArray] = React.useState<Array<ConversationObject>>([]);
    
    React.useEffect(
        () => {

            if(!conversationLoaded){

                getConversations(channelList).then((conversations : Array<ConversationObject>) => {
                    setConversationArray(conversations);
                    console.log(conversations);
                    setConversationLoaded(true);
                })

            }

        }, [conversationLoaded, channelList, conversationArray]
    )

    return(
        <div style={{borderStyle: "solid", height: '60vh' ,width:'100%',borderWidth:'1px'}}>
            <Grid container direction="column">
                <Grid item>
                    <Grid container direction='row'>
                        <Grid item>
                            <CheckBoxOutlineBlankIcon/>
                        </Grid>
                        <Grid item>
                            <ArrowDropDownIcon/>
                        </Grid>
                        <Grid item>
                            <RefreshIcon/>
                        </Grid>
                        <Grid item>
                            <MoreVertIcon/>
                        </Grid>
                        <Grid item style={{fontSize:'70%',paddingLeft:'75%',paddingTop:'5px',paddingRight:'1vw'}}>1-25 of 287</Grid>
                        <Grid item>
                            <ArrowBackIosIcon/>
                        </Grid>
                        <Grid item>
                            <ArrowForwardIosIcon/>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item>
                    
                </Grid>
            </Grid>
            <Grid container direction="row">
                {conversationArray.map((conversation, index) => (
                    <Grid item>
                        <MessageCell
                            sender={conversation.senderName!}
                            senderID={conversation.messages![0]!.fromID!}
                            conversation={conversation}
                            channelID={conversation.channelID!}
                            messageDate={new Date()}
                            message={conversation.messages![0]!.body!}
                        />
                    </Grid>
                    )
                )}
            </Grid>
        </div>
    )
}

async function getConversations(channelList : Array<string>){

    // let conversationDict : ConversationDictionary = {};

    // await channelList.forEach(async (channelID) => {

        // let conversation = await apiWrapper.get(`getConversation/${channelID}`);
        // let senderData = await apiWrapper.get(`getUser/${conversation.data[0]["fromID"]}`)
        // let senderName = senderData.data.name;

    //     let messageArray : Array<MessageObject> = conversation.data;

    //     let conversationObj : ConversationObject = {
    //         messages : messageArray,
    //         senderName : senderName
    //     }
    //     conversationDict[channelID] = conversationObj;

    // })

    // return conversationDict;

    let conversations : Array<ConversationObject> = [];

    await channelList.forEach(async (channelID) => {

        let conversation = await apiWrapper.get(`getConversation/${channelID}`);
        let senderData = await apiWrapper.get(`getUser/${conversation.data[0]["fromID"]}`)
        let senderName = senderData.data.name;

        let messageArray : Array<MessageObject> = conversation.data;

        let conversationObj : ConversationObject = {
            messages: messageArray,
            senderName : senderName,
            channelID : channelID
        };

        conversations.push(conversationObj);

    })

    return conversations;

}

export default MessagePanel;