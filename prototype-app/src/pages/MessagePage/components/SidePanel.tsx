import React from 'react';
import {Grid, Typography} from '@material-ui/core'
import Fab from '@material-ui/core/Fab';
import InboxIcon from '@material-ui/icons/Inbox';
import FlagIcon from '@material-ui/icons/Flag';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import FolderIcon from '@material-ui/icons/Folder';
import GroupIcon from '@material-ui/icons/Group';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import PersonIcon from '@material-ui/icons/Person';
import AddIcon from '@material-ui/icons/Add';

const SidePanel: React.FC<{showModal : () => void;}> = ({showModal}) => {

    return(
        <div style={{textAlign:'center'}}>
            <Grid container direction="column" justify="center" alignItems="center" spacing={0}>
                
                <Grid item>
                    <Fab onClick={showModal}  variant="extended">
                        <AddIcon></AddIcon>
                        <Typography>Create</Typography>
                    </Fab>
                </Grid>
                <Grid item justify="center" alignItems="center">
                    <Grid container direction="row" spacing={1}>
                        <Grid item style={{paddingTop:"20px", alignItems:'left'}}>
                            <InboxIcon></InboxIcon>
                        </Grid>
                        <Grid item style={{height:'100%'}}>
                            <h4>Inbox</h4>
                        </Grid>
                        <Grid item style={{paddingLeft:'40px'}}><h4> </h4>4</Grid>
                    </Grid>
                </Grid>
                <Grid item justify="center" alignItems="center">
                    <Grid container direction="row" spacing={1}>
                        <Grid item style={{paddingTop:"20px", alignItems:'left'}}>
                            <FlagIcon/>
                        </Grid>
                        <Grid item style={{height:'100%'}}>
                            <h4>Flagged</h4>
                        </Grid>
                        <Grid item style={{paddingLeft:'30px'}}><h4> </h4></Grid>
                    </Grid>
                </Grid>
                <Grid item justify="center" alignItems="center">
                    <Grid container direction="row" spacing={1}>
                        <Grid item style={{paddingTop:'20px'}}>
                            <ArrowDropDownIcon/>
                        </Grid>
                        <Grid item style={{paddingTop:"20px", alignItems:'left'}}>
                            <FolderIcon/>
                        </Grid>
                        <Grid item style={{height:'100%'}}>
                            <h4>Categories</h4>
                        </Grid>
                        <Grid item style={{paddingLeft:'30px'}}><h4> </h4></Grid>
                    </Grid>
                    <Grid container direction="column">
                        <Grid item style={{paddingLeft:'60px'}}>
                            <Grid container direction="row">
                                <Grid item style={{alignItems:'right',fontSize:'small'}}>
                                <GroupIcon/>
                                </Grid>
                                <Grid item style={{height:'100%',fontSize:'14px', paddingTop:'4px',paddingLeft:'5px'}}>
                                    Groups
                                </Grid>

                            </Grid>
                            
                        </Grid>
                        <Grid item style={{paddingLeft:'60px'}}>
                            <Grid container direction="row">
                                <Grid item style={{alignItems:'right',fontSize:'small'}}>
                                <ErrorOutlineIcon/>
                                </Grid>
                                <Grid item style={{height:'100%',fontSize:'14px', paddingTop:'4px',paddingLeft:'5px'}}>
                                    Updates
                                </Grid>

                            </Grid>
                            
                        </Grid>
                        <Grid item style={{paddingLeft:'60px'}}>
                            <Grid container direction="row">
                                <Grid item style={{alignItems:'right',fontSize:'small'}}>
                                <PersonIcon/>
                                </Grid>
                                <Grid item style={{height:'100%',fontSize:'14px', paddingTop:'4px',paddingLeft:'5px'}}>
                                    Instructors
                                </Grid>

                            </Grid>
                            
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </div>

    )

}

export default SidePanel;