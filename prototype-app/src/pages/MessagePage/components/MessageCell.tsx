import React from 'react'
import {Grid, Button} from '@material-ui/core'
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';
import moment from 'moment'
import {useHistory} from 'react-router-dom';

const MessageCell: React.FC<{
    conversation : Object,
    channelID : string,
    sender : string,
    senderID : string,
    messageDate : Date,
    message : string
}> = ({
    conversation,
    channelID,
    sender,
    senderID,
    messageDate,
    message
}) => {

    const history = useHistory();

    const cellClicked = () => {

        history.push(
            {
                pathname: `/messages/${channelID}`,
                state: {
                    conversation: conversation
                }
            }
        )

    }

    let dateString = moment(messageDate).format('LL');

    console.log("Attempting cell")

    return (
        <Button onClick={cellClicked}>
            <div style={{width: '100%', height: '5vh', backgroundColor: "white"}}>
                <Grid container direction="row" justify="flex-start" alignItems="center">
                    <Grid item>
                        <CheckBoxOutlineBlankIcon/>
                    </Grid>
                    <Grid item style={{paddingLeft:'20px'}}>
                        <h5>{sender}</h5>
                    </Grid>
                    <Grid item style={{paddingLeft:'5vw'}}>
                        <p>{message}</p>
                    </Grid>
                    <Grid item style={{paddingLeft:'30vw'}}>
                        <h6>{dateString}</h6>
                    </Grid>
                </Grid>
            </div>
        </Button>
    )

}

export default MessageCell;