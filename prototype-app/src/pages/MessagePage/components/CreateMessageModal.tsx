import React from 'react';
import {Dialog, Grid, Select, MenuItem, TextField, IconButton, Button, Typography, Snackbar} from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close';
import { Alert } from '@material-ui/lab';
import {useAuth} from '../../../contexts/AuthContext';
import {Class} from '../../../models/class.model';
import apiWrapper from '../../../utils/apiWrapper';

const CreateMessageModal: React.FC<{isOpen: boolean; closeModal: () => void;}> = ({isOpen, closeModal}) => {

    //Add an X button
    //Add a send button
    //Send snackbar

    const [classSelected, setClassSelected] = React.useState(false);
    const [recipientSelected, setRecipientSelected] = React.useState(false);
    const [classChoice, setClassChoice] = React.useState<Class>({});
    const [recipientChoice, setRecipientChoice] = React.useState('');
    const [recipientIsTeacher, setRecipientIsTeacher] = React.useState(false);
    const [bodyText, setBodyText] = React.useState('');
    const [showSnackbar, setShowSnackbar] = React.useState(false);

    const [nameDictionary, setNameDictionary] = React.useState({});
    const [namesLoaded, setNamesLoaded] = React.useState(false);

    const { storedUserData } = useAuth();
    const classData : Array<Class> = storedUserData.classData;

    let teacherName = "Johannes James";
    let nameList = [
        "James Winston",
        "Steve Williams",
        "Craig Johnson",
        "Jeanine Billow",
        "Frank Billingsly"
    ]

    const handleClassChange = (event) => {

        const classIndex = event.target.value;
        setClassChoice(classData[classIndex - 1]);
        setClassSelected(true);
        setRecipientSelected(false);
        setRecipientChoice('');
    };
    
    const handleStudentChange = (event) => {

        const studentIndex = event.target.value;
        if(studentIndex === -1){

            setRecipientChoice(classChoice.teacherID!);
            setRecipientSelected(true);
            setRecipientIsTeacher(true);

        }else{

            const studentID = classChoice.studentList![studentIndex - 2];
            setRecipientChoice(studentID);
            setRecipientSelected(true);
            setRecipientIsTeacher(false);

        }

    }

    const handleBodyChange = (event) => {
        setBodyText(event.target.value);
    }

    const handleDialogClose = () => {
        closeModal();
    }

    const handleClose = (event: any, reason: any) => {
		if (reason === 'clickaway') {
			return;
		}
		setShowSnackbar(false);
	};

    const sendPressed = async() => {

        const payload = {
            existingChannel : false,
            toID: recipientChoice,
            fromID: storedUserData.uid,
            toIsTeacher: recipientIsTeacher,
            fromIsTeacher : storedUserData.isTeacher,
            body: bodyText
        };

        await createNewMessage(payload);

        setShowSnackbar(true);
        closeModal();
    }

    React.useEffect(
        () => {

            if(!namesLoaded){


                getClassNames(classData).then((dictionary) => {
                    console.log(dictionary);
                    setNameDictionary(dictionary);
                    setNamesLoaded(true);
                })


            }


        }, [namesLoaded, classData, nameDictionary]
    )

    return (
        <div>
            {
                (namesLoaded) ? (
                    <Dialog 
                    open={isOpen}
                    fullWidth={true}
                    maxWidth={'md'}
                    >
                    <Grid container direction="row" justify="space-between" alignItems="center">
                        <Grid item>
                            <h2 style={{paddingLeft: '5vw'}}>Create a New Message!</h2>
                        </Grid>
                        <Grid item>
                            <div style={{paddingLeft: '5vw'}}>
                                <IconButton style={{paddingRight: '5vw'}} aria-label="close" onClick={handleDialogClose}>
                                    <CloseIcon />
                                </IconButton>
                            </div>
                        </Grid>
                    </Grid>
                    <Grid container direction="column" justify="center" alignItems="center">
                        <Grid item>
                            <div style={{width: '50vw'}}>
                                <Grid container direction="row" justify="space-between" alignItems="center">
                                    <Grid item>
                                        <p>Select Class: </p>
                                    </Grid>
                                    <Grid item>
                                        <Select style={{width: '20vw'}} labelId="demo-simple-select-label" id="demo-simple-select" onChange={handleClassChange}>
                                            {
                                                classData.map((singleClass, index) => (
                                                    <MenuItem key={`menu-item-${index}`} value={index + 1}>{singleClass.className}</MenuItem>
                                                ))
                                            }
                                        </Select>
                                    </Grid>
                                </Grid>
                            </div>
                        </Grid>
                        {
                            (classSelected) ? (
                                <Grid item>
                                    <div style={{width: '50vw'}}>
                                        <Grid container direction="row" justify="space-between" alignItems="center">
                                            <Grid item>
                                                <p>Select from Class list: </p>
                                            </Grid>
                                            <Grid item>
                                                <Select value={recipientChoice} style={{width: '20vw'}} native defaultValue="" id="grouped-native-select" onChange={handleStudentChange}>
                                                    <option aria-label="None" value="" />
                                                    <optgroup label="Teacher">
                                                        <option value={-1}>{teacherName}</option>
                                                    </optgroup>
                                                    <optgroup label="Student">
                                                        {
                                                            nameList.map((student, index) => (
                                                                <option value={index + 2}>{student}</option>
                                                            ))
                                                        }
                                                    </optgroup>
                                                </Select>
                                            </Grid>
                                        </Grid>
                                    </div>
                                </Grid>
                            ) : (undefined)
                        }
                        {
                            (classSelected && recipientSelected) ? (
                                <Grid item>
                                    <div style={{marginBottom: '5vh', marginTop: '5vh'}}>
                                        <TextField
                                            id="outlined-multiline-static"
                                            label="Message Body"
                                            multiline
                                            rows={4}
                                            variant="outlined"
                                            style={{width: '50vw'}}
                                            onChange={handleBodyChange}
                                        />
                                        <br/>    
                                        <div style={{marginTop: '5vh'}}>
                                            <Button onClick={sendPressed}>
                                                <Typography>Send Message</Typography>
                                            </Button>
                                        </div>                            
                                    </div>
                                </Grid>
                            ) : (undefined)
                        }
                    </Grid>
                </Dialog>
                ) : (undefined)
            }
            <Snackbar open={showSnackbar} autoHideDuration={6000} onClose={handleClose}>
                <Alert severity="success">Direct Message sent!</Alert>
            </Snackbar>
        </div>
    )

}

async function createNewMessage(messageObj){
    apiWrapper.post('createMessage', messageObj).then(() => {
        return;
    })
}

async function getClassNames(classes : Array<Class>){

    let listDictionary = {};
    await classes.forEach(async (singleClass, index) => {

        let classID = singleClass.classID!;

        let classList = await apiWrapper.get(`getClassList/${classID}`);

        console.log(classList.data);

        listDictionary[classID] = classList.data;

    })

    return listDictionary;
}

export default CreateMessageModal;