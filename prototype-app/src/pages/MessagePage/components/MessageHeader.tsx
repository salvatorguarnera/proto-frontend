import React from 'react';
import {Grid} from '@material-ui/core'
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import SearchIcon from '@material-ui/icons/Search';
import SwipeableSideBar from '../../../components/SwipeableSideBar/SwipeableSideBar'

const MessageHeader = () => {

    return(

        <div style={{width: '100vw', overflowX: 'hidden'}}>

            <Grid container direction="row" justify="flex-start" alignItems="center" spacing={4}>
                <Grid item style={{paddingRight:'0px'}}><SwipeableSideBar></SwipeableSideBar></Grid>
                <Grid item style={{paddingTop:'30px',paddingLeft:'0px'}}>
                    <h2>Messages</h2>
                </Grid>
                <Grid item >
                <Grid container spacing={0} alignItems="flex-end">
                    
                    <Grid item style={{width:'65vw',paddingLeft:'3px'}}>
                        <TextField 
                            id="input-with-icon-grid"
                            label="Search Messages"
                            variant="filled"
                            InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                    <SearchIcon />
                                    </InputAdornment>
                                    ),
                                }} 
                                fullWidth
                    />
                    </Grid>
                    </Grid>
                </Grid>
            </Grid>

        </div>

    )

}

export default MessageHeader;