import React from 'react';
import ClassHeader from './components/ClassHeader';
import Announcements from './components/Announcements';
import Calendar from '../../components/Calendar/Calendar';
import {Grid} from '@material-ui/core';
import {useHistory, useParams} from 'react-router-dom';


const ClassPage = () => {

    const history = useHistory();
    const {classID} = useParams();
    function assignmentsPressed(){
        history.push(`/classes/${classID}/assignments`);
    }

    function classListPressed(){
        history.push(`/classes/${classID}/classlist`);
    }

    return (
        <div>
            <div style={{width: '100vw', overflowX: "hidden"}}>
                <ClassHeader className={"Name"} assignmentsPressed={assignmentsPressed} classListPressed={classListPressed}/>
                <div style={{marginLeft: '10vw'}}>
                    <Grid container direction="row" justify="flex-start" alignItems="flex-start">
                        <Grid item>
                            <Announcements/>
                        </Grid>
                        <Grid item>
                            <Calendar/>
                        </Grid>
                    </Grid>
                </div>
            </div>
        </div>
    )

}

export default ClassPage;