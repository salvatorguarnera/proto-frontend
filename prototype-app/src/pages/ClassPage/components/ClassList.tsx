import React from 'react'
import {Grid} from '@material-ui/core';
import {useParams} from 'react-router-dom';
import apiWrapper from '../../../utils/apiWrapper';
import SwipeableSideBar from '../../../components/SwipeableSideBar/SwipeableSideBar'

const ClassList = () => {

    const {classID} = useParams();
    
    const [listLoaded, setListLoaded] = React.useState(false);
    const [nameList, setNameList] = React.useState<Array<string>>([]);

    React.useEffect(
        () => {

            if(!listLoaded){
                
                apiWrapper.get(`getClassList/${classID}`).then((res) => {

                    setListLoaded(true);
                    setNameList(res.data);
                })

            }
        },
        [listLoaded, nameList, classID]
    )

    return(
        <div>
            <SwipeableSideBar/>
            <Grid container direction="column" justify="center" alignItems="center" style={{width : '100vw'}}>
                <Grid item>
                    <h3>List of Class Members</h3>
                </Grid>
                {
                    (listLoaded) ? (
                        nameList.map((name, index) => (
                            <Grid item>
                                <h3>{name}</h3>
                            </Grid>    
                        ))
                    ) : (undefined)
                }
            </Grid>
        </div>
    )
}


// async function getClassData(classID: string, classData : Array<Class>){
//     return "hi";
// }


export default ClassList;