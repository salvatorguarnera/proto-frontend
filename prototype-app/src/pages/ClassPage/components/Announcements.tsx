import React from 'react';
import {Grid, Divider, Button, Typography} from '@material-ui/core';
import {useHistory} from 'react-router-dom';

const Announcements = () => {

    const history = useHistory();
    function messagesClicked(){
        history.push('/messages');
    }

    return (

        <div style={{width: '55vw', height: 'auto', borderStyle: 'solid'}}>

            <Grid container direction="column" justify="flex-start" alignItems="flex-start">
                <Grid item>
                    <div style={{width: '50vw'}}>
                        <h2>Announcements</h2>
                        <Divider/>
                    </div>
                </Grid>
                <Grid item>
                    <div style={{width: "50vw", height: '25vh'}}>
                        <p>Due to the events that occurred this week, I decided to push back the due date of Lab 4.  This does not change any due dates of future assignments.  Please remember to still turn in your lab for this week.</p>
                    </div>
                </Grid>
                <Grid item>
                    <div style={{width: "50vw"}}>
                        <Divider/>
                        <Button onClick={messagesClicked}>
                            <Typography>Go to Messages</Typography>
                        </Button>
                    </div>
                </Grid>
            </Grid>

        </div>

    )
}

export default Announcements;