import React from 'react';
import {Grid, Divider} from '@material-ui/core';
import {useParams} from 'react-router-dom';
import {useAuth} from '../../../contexts/AuthContext';
import {Class} from '../../../models/class.model';
import CourseSelect from '../../../components/CourseSelect/CourseSelect';
import SwipeableSideBar from '../../../components/SwipeableSideBar/SwipeableSideBar';
const ClassHeader : React.FC< 
{
    className : string;
    assignmentsPressed: () => void;
    classListPressed: () => void;
} 
> = (
{
    className, 
    assignmentsPressed, 
    classListPressed,
}) => {
    const {classID} = useParams(); 
    const { storedUserData } = useAuth();
    const classData : Array<Class> = storedUserData.classData;
    var classObj = classData.find((a)=>{return a.classID === classID});

    

   
    return (
        <div >
            <div style={{fontSize: "1.5vw"}}>
                <Grid container direction="row" justify="space-between" alignItems="center">
                    <Grid item><SwipeableSideBar/></Grid>
                    <Grid item>
                        <div style={{paddingLeft: '10vw'}}>
                            <h2>{classObj?.className}</h2>
                        </div>
                    </Grid>
                    <Grid item>
                    <CourseSelect></CourseSelect>
                    </Grid>
                    <Grid item>

                    </Grid>
                </Grid>
            </div>
            <div style={{marginLeft: '10vw'}}>
                <Grid container direction="row" spacing={3}>
                    <Grid item>
                        <p>Course Home</p>
                    </Grid>
                    <Grid item>
                        <button onClick={assignmentsPressed}>
                            <p>Assignments</p>
                        </button>
                    </Grid>
                    <Grid item>
                        <p>Quizzes</p>
                    </Grid>
                    <Grid item>
                        <p>Grades</p>
                    </Grid>
                    <Grid item>
                        <button onClick={classListPressed}>
                            <p>Classlist</p>
                        </button>
                    </Grid>
                    <Grid item>
                        <p>More</p>
                    </Grid>
                </Grid>
            </div>
            <Divider/>
        </div>

    )

}

export default ClassHeader;