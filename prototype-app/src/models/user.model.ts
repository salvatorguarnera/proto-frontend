import {Class} from './class.model';

export interface User {
    email? : string;
    isTeacher? : boolean;
    name? : string;
    uid? : string;
    classList? : Array<string>;
    classData? : Array<Class>;
}