export interface Class{
    classCode? : string,
    classID?: string,
    className?: string,
    studentList?: Array<string>,
    teacherID?: string
};