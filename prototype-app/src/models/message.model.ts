export interface MessageObject{
    body? : string,
    toID? : string,
    fromID? : string,
    timestamp? : {
        _seconds : number,
        _milliseconds : number
    },
    read? : boolean
}

export interface ConversationObject{
    messages? : Array<MessageObject>,
    senderName? : string,
    channelID? : string
}

export interface ConversationDictionary{
    [channelID : string]: ConversationObject;
}