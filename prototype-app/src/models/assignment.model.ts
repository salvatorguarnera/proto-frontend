export interface Assignment{
    homeworkName?: string,
    fileLink?: string,
    description?: string,
    dueDateTime?: Date
}