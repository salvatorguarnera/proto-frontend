import firebase from "firebase/app"
import "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyBWHP4c4BxgUfy7yl93DFckaHIUUpJV-xA",
    authDomain: "prototypeapp-28f76.firebaseapp.com",
    databaseURL: "https://prototypeapp-28f76-default-rtdb.firebaseio.com",
    projectId: "prototypeapp-28f76",
    storageBucket: "prototypeapp-28f76.appspot.com",
    messagingSenderId: "439116562010",
    appId: "1:439116562010:web:a9c1573863df921e58e263",
    measurementId: "G-FBGYJ08662"
};

const app = firebase.initializeApp(firebaseConfig);

export const auth = app.auth()
export default app;