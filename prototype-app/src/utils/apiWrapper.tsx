import axios from 'axios';

export default axios.create({
	baseURL: 'https://us-central1-prototypeapp-28f76.cloudfunctions.net/api/'
});