import React from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { useState } from 'react'
import { useAuth } from "../../contexts/AuthContext"
import { useHistory } from "react-router-dom"
import {Button, Grid, Typography} from '@material-ui/core'
import SideBarContainer from '../SideBarContainer/SideBarContainer'
import styles from './SideBar.module.css';



  
export default function SideBar() {
    
    var event = new CustomEvent("LogoutSuccess");
    
    const [error, setError] = useState("")
    const { logout } = useAuth()
    const history = useHistory()
    const [open, setOpen] = React.useState(false);



    // React.useEffect(
    //     () => {

    //         if(!logoLoaded){
    //             refToImage.getDownloadURL().then((url)=>{
    //                 var yo = document.getElementById("frontLogo")
    //                 yo?.setAttribute('src',url);
    //                 setLogoLoaded(true);
    //             }).catch((error)=>{
    //                 console.error(error);
    //             })
    //         }

    //     }, [logoLoaded, refToImage]
    // )


    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
  

    async function handleLogout() {
        setError("")

        try {
            await logout()
            history.push("/login")
            console.log(history);
            document.dispatchEvent(event);
            
        } catch {
            setError("Failed to log out")
            console.error(error)
        }
    }


    return (
        <div style={{color: 'black'}}>
            <Grid container direction="column" justify="center" alignItems="center" spacing={2}>
                <Grid item>
                    <img 
                        id={"frontLogo"} 
                        alt='logoPic' 
                        src={"https://firebasestorage.googleapis.com/v0/b/prototypeapp-28f76.appspot.com/o/logos%2Flogo3.png?alt=media&token=64b9ee8c-b5a5-4ab4-b7c6-8bb428a9ef46"}
                        className={styles.center}/>
                </Grid>
                <Grid item>
                    <SideBarContainer itemName={"Dashboard"} path={""}/>
                </Grid>
                <Grid item>
                    <SideBarContainer itemName={"Calendar"} path={"calendar"}/>
                </Grid>
                <Grid item>
                    <SideBarContainer itemName={"Messages"} path={"messages"}/>
                </Grid>
                <Grid item>
                    <Button onClick={handleClickOpen} className={styles.center}>
                        <Typography>Logout</Typography>
                    </Button>
                    <Dialog
                        open={open}
                        onClose={handleClose}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogTitle id="alert-dialog-title">{"Are you sure?"}</DialogTitle>
                        <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            This action will log you out of your current session.
                        </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                        <Button onClick={handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={handleLogout} color="primary" autoFocus>
                            Yes
                        </Button>
                        </DialogActions>
                    </Dialog>
                    
                </Grid>
            </Grid>
        </div>
    )
}
