import React from 'react'
import {Grid, Divider} from '@material-ui/core';
import styles from './DashHeader.module.css';

export default function DashHeader(props) {
    
    return (
        <div className={styles.dashHeader}>
            <Grid
            container
            direction="column"
            justify="flex-start"
            alignItems="flex-start"
            >
                <Grid item>
                    <div style={{width: '80vw'}}>
                        <h1>Dashboard</h1>
                        <p>Good afternoon, {props.userData["name"]}</p>
                        <Divider/>
                    </div>
                </Grid>
                <Grid item>
                    <div style={{width: '80vw'}}>
                        <Grid
                        container
                        direction="row"
                        justify="space-between"
                        alignItems="center"

                        >
                            <Grid item>
                                <h4>Past Courses</h4>
                            </Grid>
                            <Grid item>
                                <h2>Current Courses</h2>
                            </Grid>
                            <Grid item>
                                <h4>Upcoming Courses</h4>
                            </Grid>
                        </Grid>
                    </div>
                    <Divider/>
                </Grid>
            </Grid>
        </div>
    )
    
}
