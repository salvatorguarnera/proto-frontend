import React, { useState } from 'react'
import { Button, Typography, Grid, TextField } from '@material-ui/core'
import { useAuth } from '../../contexts/AuthContext';
import { useHistory } from 'react-router-dom'
import styles from './Login.module.css';
import {isEmailValid} from '../../utils/validators';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';


function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }


export default function Login() {


    // refToImage.getDownloadURL().then((url)=>{
    //     var yo = document.getElementById("fLogo")
    //     yo?.setAttribute('src',url);
    // }).catch((error)=>{
    //     console.error(error);
    // })

    const { login } = useAuth()
    // const [loading, setLoading] = useState(false)
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const[emailIsValid, setEmailIsValid] = useState(true)
    const [passwordError, setPasswordError] = useState(false)
    const [open, setOpen] = React.useState(false);

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
        return;
        }
        setOpen(false);
    };

    const history = useHistory()

    const handleSubmit = async () => {
		if (email.trim().length !== 0 && isEmailValid(email)) {
            
            try{
                await login(email, password)
                history.push("/")
                resetValues()
            }catch{
                setPasswordError(true)
            }
		} else {
			if (!isEmailValid(email)) {
				setEmailIsValid(false);
			}
		}
	};

    const onEnterPress = (e) => {
        if (e.key === 'Enter') {
          handleSubmit();
        }
    }

    const handleEmailChange = (event) => {
		setEmail(event.target.value)
	};

	const handlePasswordChange = (event) => {
		setPassword(event.target.value)
    };
    
    const resetValues = () => {
		setEmail('');
		setPassword('');
        setEmailIsValid(true);
        setPasswordError(false)
        // setLoading(false)
    };
    //If coming from a successful logout, it will display a badge (see Sidebar.js for event beginning)
    document.addEventListener("LogoutSuccess", function(e) {
        setOpen(true); // Prints "Example of an event"
      });
    
    
    return (
        <div style={{height: '100vh', width: '100vw', alignItems: 'center'}}>
            <div style={{width: '50vw', marginLeft: 'auto', marginRight: 'auto', paddingTop: '15Vh'}}>
                <Grid
                container
                direction="column"
                justify="center"
                alignItems="center"
                spacing={3}
                >
                    <Grid item>
                        <div className={styles.logoImage}>
                                <img 
                                id={"fLogo"} 
                                src={"https://firebasestorage.googleapis.com/v0/b/prototypeapp-28f76.appspot.com/o/logos%2Flogo6.png?alt=media&token=09e97b27-947e-42a8-a6d7-7726553f4d41"}
                                alt="ChalkBoard logo"/>
                        </div>
                    </Grid> 

                    <Grid item>
                        <h3 className={styles.h3}>
                            Welcome to ChalkBoard!
                        </h3>
                    </Grid>
                    <Grid item xs={12}>
                        <TextField 
                            id="standard-basic" 
                            label="Email address" 
                            style={{width: '20vw'}}
                            onChange={handleEmailChange} 
                            onKeyPress={onEnterPress}
                            error={!emailIsValid}
							helperText={
                                !emailIsValid ? (
                                    'Please enter a valid email address'
                                ) : null
                            } 
                        />
                    </Grid>
                    <Grid item>
                        <TextField 
                            id="standard-basic" 
                            label="Password" 
                            style={{width: '20vw'}} 
                            type="password"
                            onChange={handlePasswordChange} 
                            onKeyPress={onEnterPress}
                            error={passwordError}
                            helperText={
                                passwordError ? ('Password is incorrect')
                                :
                                null
                            }
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Button className={styles.loginButton} variant="contained" onClick={handleSubmit}>
                            <Typography>Login</Typography>
                        </Button>
                    </Grid>
                </Grid>
                
                <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                    <Alert onClose={handleClose} severity="success">
                        Logout Success!
                    </Alert>
                </Snackbar>
            </div>
        </div>
        
    )
}

