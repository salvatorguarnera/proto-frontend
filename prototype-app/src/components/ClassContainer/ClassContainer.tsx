import React from 'react'
import {Grid, Typography} from '@material-ui/core'
import {User} from '../../models/user.model';
import ClassComponent from '../ClassComponent/ClassComponent';
import {Class} from '../../models/class.model';

const ClassContainer : 
React.FC<
{ 
    user : User;
    teacherDict;
}
> = ({user, teacherDict}) => {

    let listOfClassData = splitClasses(user.classData!);

    return (
        <div style={{width: '55vw'}}>
            <Grid container direction="column">

                {
                    (listOfClassData) && (listOfClassData.length > 0) ? (
                        
                        listOfClassData.map((subList, index) => (
                            <div style={{marginBottom: '3vh'}}>
                                <Grid container direction="row" spacing={3}>
                                    {subList.map((singleClass, singleindex) => (
                                        <ClassComponent key={`class-component-${singleindex}`} classData={singleClass} teacherDict={teacherDict} elementIndex={`${index + singleindex}`}/>
                                    ))}
                                </Grid>
                            </div>
                        ))
                        
                    )
                    :
                    (
                        <Grid container direction="column">
                            <Grid item>
                                <Typography>You have no classes!</Typography>
                            </Grid>
                        </Grid>
                    )
                }

            </Grid>
        </div>
    )
    
}

function splitClasses(currentClassArray : Array<Class>){

    let twoDimensionalArray : Array<Array<Class>> = [];

    if(currentClassArray && currentClassArray.length > 0){

        for(let i = 0; i < currentClassArray.length; i += 2){

            let subArray : Array<Class> = [];
            let j = i;
            while(j < (i + 2) && j < currentClassArray.length){
                subArray.push(currentClassArray[j]);
                j++;
            }
            twoDimensionalArray.push(subArray);
        }

    }

    return twoDimensionalArray;

}

export default ClassContainer;
