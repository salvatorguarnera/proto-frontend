import React from 'react'
import {Button, Grid} from '@material-ui/core'
import {useHistory} from 'react-router-dom';

const Calendar = () => {
    
    const history = useHistory();

    function calendarClicked(){
        history.push("/calendar")
    }

    let assignmentObj = {
        "March 15th" : [
            {
                title : "Software Projects Final Demo",
                time : "11:59PM"
            }
        ],
        "March 18th" : [
            {
                title : "Object Oriented Quiz",
                time : "11:59PM"
            }
        ],
        "March 21st" : [
            {
                title : "Assignment 8",
                time : "11:59PM"
            }
        ],
        "March 24th" : [
            {
                title : "Discussion 3",
                time : "2:00pm"
            }
        ]
    }

    // let myEventsList = [
    //     {
    //         id: 0,
    //         title: 'Software Projects Final Demo',
    //         allDay: true,
    //         start: new Date(2021, 3, 15),
    //         end: new Date(2021, 3, 15)
    //     },
    //     {
    //         id: 1,
    //         title: 'Object Oriented Quiz',
    //         allDay : true,
    //         start : new Date(2021, 3, 18),
    //         end: new Date(2021, 3, 18)
    //     },
    //     {
    //         id: 1,
    //         title: 'Assignment 8',
    //         allDay : true,
    //         start : new Date(2021, 3, 21),
    //         end: new Date(2021, 3, 21)
    //     },
    //     {
    //         id: 1,
    //         title: 'Quiz 4',
    //         allDay : true,
    //         start : new Date(2021, 3, 24),
    //         end: new Date(2021, 3, 24)
    //     }
    // ]

    return (
        <Button onClick={calendarClicked}>
            <div style={{width: '20vw', height: '65vh', borderStyle: 'solid', overflow: 'auto'}}>
                <h1 style={{textDecoration: 'underline'}}>Calendar</h1>
                <Grid container direction="column" justify="center" alignItems="center">
                    {
                        Object.keys(assignmentObj).map((key, index) => (
                            <Grid item>
                                <div>
                                    <h3>{key}</h3>
                                    {assignmentObj[key].map((assignment, assignmentIndex) => (
                                        <p>{`${assignment.title} due ${assignment.time}`}</p>
                                    ))}
                                </div>
                            </Grid>
                        ))
                    }
                </Grid>
            </div>
        </Button>
    )
    
}

export default Calendar
