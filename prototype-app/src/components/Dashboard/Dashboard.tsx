import React, {useState} from 'react'
import {Grid} from '@material-ui/core'
import styles from './Dashboard.module.css';
import SideBar from '../SideBar/SideBar';
import DashHeader from '../DashHeader/DashHeader';
import ClassContainer from '../ClassContainer/ClassContainer';
import Calendar from '../Calendar/Calendar';
import { useAuth } from "../../contexts/AuthContext";
import apiWrapper from "../../utils/apiWrapper";
import LoadingModal from '../../components/LoadingModal/LoadingModal';
import { User } from '../../models/user.model';
import {Class} from '../../models/class.model';

const Dashboard = () => {

    const {currentUser, applyStoredUserData} = useAuth();
    const [userData, setUserData] = React.useState<User>({});
    const [isUserLoaded, setIsUserLoaded] = useState(false);
    const [areClassesLoaded, setAreClassesLoaded] = useState(false);
    const [teacherNameDictionary, setTeacherNameDictionary] = React.useState({});

    React.useEffect(
      () => {

        if(!isUserLoaded){

          apiWrapper.get(`getUser/${currentUser["uid"]}/`).then((res) => {
            let obj : User = {...res.data};
            setUserData(obj);
            setIsUserLoaded(true);
          });

        }

      },
      [ isUserLoaded, currentUser]
    )

    React.useEffect(

      () => {

        if(isUserLoaded && !areClassesLoaded){

          let payload = {
            userID: userData.uid!,
            isTeacher: userData.isTeacher!
          };

          apiWrapper.post("getClass", payload).then((res) => {

            const classArray : Array<Class> = [];
            
            for(let key in res.data){
              classArray.push( {...res.data[key]} );
            }
            
            let newUserData : User = {
              ...userData,
              classData : classArray
            };

            setUserData(newUserData);
            applyStoredUserData(newUserData);
            return classArray;
          })
          .then((classArray) => {

            return getTeacherNames(classArray);

          })
          .then((teacherSnapshots) => {

            let teacherDict = {}
            teacherSnapshots.forEach(function(snapshot, index){
              
              const uid = snapshot.data.uid;
              const name = snapshot.data.name;

              teacherDict[uid] = name;

            })

            setTeacherNameDictionary(teacherDict);
            setAreClassesLoaded(true);

          })
          .catch((err) => {

            //User has no classes...
            let newUserData : User = {
              ...userData,
              classData : []
            }
            
            setAreClassesLoaded(true);
            setUserData(newUserData);
            
          })

        }

      },
      [isUserLoaded, userData, areClassesLoaded, applyStoredUserData]

    )

    return (

      <div>
        {isUserLoaded && areClassesLoaded ? (
          <div>
            <div className={styles.sidebar}>
              <SideBar/>
            </div>
            <div className={styles.mainContainer}>
              <Grid
                container
                direction="column"
                justify="flex-start"
                alignItems="flex-start"
                spacing={2}
              >
                <Grid item>
                  <DashHeader userData={userData}/>
                </Grid>
                <Grid item>
                  <div>
                    <Grid container direction="row" justify="flex-start" alignItems="flex-start" spacing={2}>
                      <Grid item>
                        <ClassContainer user={userData} teacherDict={teacherNameDictionary}/>
                      </Grid>
                      <Grid item>
                        <Calendar/>
                      </Grid>
                    </Grid>
                  </div>
                </Grid>
              </Grid>
            </div>
          </div>
        ) : 
          (
            <LoadingModal isOpen={!isUserLoaded} />
          )
        }
      </div>

    );
}

async function getTeacherNames(classData : Array<Class>){

  const promiseArray : Array<Promise<any>> = [];
  const calledSet = new Set();

  for(const singleClass of classData){

    let teacherID = singleClass.teacherID;
    if(!calledSet.has(teacherID)){

      let promise = apiWrapper.get(`getUser/${teacherID}`);
      promiseArray.push(promise);
      calledSet.add(teacherID);

    }

  }

  return Promise.all(promiseArray);

}

export default Dashboard
