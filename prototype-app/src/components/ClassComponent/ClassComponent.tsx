import React from 'react';
import {Class} from '../../models/class.model'
import {Button} from '@material-ui/core'
import styles from './ClassComponent.module.css'
import {useHistory} from 'react-router-dom';
import 'firebase/storage';


const ClassComponent : React.FC <
{
    classData : Class, 
    elementIndex : string,
    teacherDict,
}
> = ({classData, elementIndex, teacherDict}) => {
    
    const history = useHistory();
    const imagetag = "imgtag"+elementIndex;

    let imageArray = [
        "https://firebasestorage.googleapis.com/v0/b/prototypeapp-28f76.appspot.com/o/classPhotos%2FclassComp1.jpg?alt=media&token=b17b3455-6d5b-49e8-87ed-d3db824c3f02",
        "https://firebasestorage.googleapis.com/v0/b/prototypeapp-28f76.appspot.com/o/classPhotos%2FclassComp12.jpg?alt=media&token=8f19aa59-e220-4bbb-ba93-55210e8fe377",
        "https://firebasestorage.googleapis.com/v0/b/prototypeapp-28f76.appspot.com/o/classPhotos%2FclassComp13.jpg?alt=media&token=2668ee30-dc58-480f-b1ba-3b2ab1403831",
        "https://firebasestorage.googleapis.com/v0/b/prototypeapp-28f76.appspot.com/o/classPhotos%2FclassComp16.jpg?alt=media&token=e6fc3e58-b938-4567-bd04-967d8c4326a2"
    ]

    // React.useEffect(
    //     () => {

    //         if(!imageLoaded){

    //             refToImage.getDownloadURL().then((url)=>{
    //                 var yo = document.getElementById(imagetag)
    //                 yo?.setAttribute('src',url);
    //                 setImageLoaded(true);
    //             }).catch((error)=>{
    //                 console.error(error);
    //             })

    //         }

    //     }, [imageLoaded, refToImage]
    // )

    function classClicked(classID : string){
        history.push(`/classes/${classID}`);
    }
    return (
        
        <Button onClick={ () => {classClicked(classData.classID!)} }>
            <div style={{width: '25vw', height: '25vw', backgroundColor:'white'}}>
                <img id={imagetag} className={styles.image} src={imageArray[elementIndex]} alt=""></img>
                <div className={styles.infoContainer} style={{width : '25vw', height: 'auto', backgroundColor: 'white'}}>
                    <h3 className ={styles.className}>{classData.className!}</h3>
                    <p>{`Professor ${teacherDict[classData.teacherID!]}`}</p>
                </div>
            </div>
        </Button>
    )

}

export default ClassComponent;