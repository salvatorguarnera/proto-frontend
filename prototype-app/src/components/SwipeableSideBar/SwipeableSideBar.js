import React from 'react';
import {Button, Grid} from '@material-ui/core'
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import MenuIcon from '@material-ui/icons/Menu';
import SideBar from '../SideBar/SideBar'

export default function SwipeableSideBar() {
    
    const [state,setState]= React.useState({
        left:false,
    });
    const toggleDrawer = (open) => (event) => {
        if (event && event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
          return;
        }
    
        setState({ ...state, left: open });
      };


    return (
        <div style={{color: 'black'}}>
            <React.Fragment key={'left'}>
            <Button onClick={toggleDrawer(true)}>
                <MenuIcon></MenuIcon>
            </Button>
            <Grid container direction="column" justify="center" alignItems="center" spacing={2}>
            <SwipeableDrawer
            anchor={'left'}
            open={state['left']}
            onClose={toggleDrawer(false)}
            onOpen={toggleDrawer(true)}
          >
                <SideBar></SideBar>
                </SwipeableDrawer>
            </Grid>
            </React.Fragment>
        </div>
    )
}
