import React from 'react';
import styles from './SideBarContainer.module.css';

import { Box, Button } from '@material-ui/core';
import {useHistory} from 'react-router-dom';

const SideBarContainer: React.FC<{itemName : string, path: string}> = ({itemName, path}) => {
    
    const history = useHistory();
    function dashItemClicked(){
        history.push(`/${path}`);
    }

    return (
        <Button onClick={dashItemClicked}>
            <div className={styles.SideTitle}>
                <Box boxShadow={3}>
                    <div style={{height: '5vw', width: '10vw'}}>
                        <p className={styles.insideText}>{itemName}</p>
                    </div>
                </Box>
            </div>
        </Button>
    )
    
}

export default SideBarContainer
