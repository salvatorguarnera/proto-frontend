import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import {useHistory} from 'react-router-dom';
import {useAuth} from '../../contexts/AuthContext';
import {Class} from '../../models/class.model';

export default function CourseSelect() {
    const history = useHistory() 
    const { storedUserData } = useAuth();
    const classData : Array<Class> = storedUserData.classData;
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };
    function classClicked(classID : string){
        history.push(`/classes/${classID}`);
    }
    return (
        <div style={{paddingLeft: '15vw'}}>
            <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
                <h3>Select a Course</h3>
            </Button>
            <Menu
                id="simple-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <MenuItem onClick={()=>{history.push('/')}}>Dashboard</MenuItem>
                {classData.map((c, index) => (
                    <MenuItem key={`menu-item-${index}`} onClick={() => {
                        classClicked(c.classID!);
                        handleClose()}}> 
                        {c.className} 
                    </MenuItem>
                    ))}
                </Menu>
        </div>
    )
}
