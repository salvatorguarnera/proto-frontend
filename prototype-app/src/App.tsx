import './App.css';
import axios from 'axios';
import {Container} from 'react-bootstrap';
import { AuthProvider } from './contexts/AuthContext';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import PrivateRoute from "./components/PrivateRoute/PrivateRoute";
import Login from './components/Login/Login';
import Dashboard from './components/Dashboard/Dashboard';
import ClassPage from './pages/ClassPage/ClassPage';
import CalendarPage from './pages/CalendarPage/CalendarPage';
import MessagePage from './pages/MessagePage/MessagePage';
import AssignmentsPage from './pages/AssignmentsPage/AssignmentsPage';
import ClassList from './pages/ClassPage/components/ClassList';
import ConversationPage from './pages/MessagePage/components/ConversationPage';
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles';
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';

  
  axios.defaults.baseURL = 
  "https://us-central1-prototypeapp-28f76.cloudfunctions.net/api";

  const theme = createMuiTheme({
    palette: {
      primary: {
        light: '#4f5b62',
        main: '#263238',
        dark: '#000a12',
        contrastText: '#000'
      },
      secondary: {
        light: '#9d46ff',
        main: '#6200ea',
        dark: '#0a00b6',
        contrastText: '#000'
      }
    },
    typography: {
      h4: {
        fontFamily: 'Arial',
        fontWeight: 900
      },
      subtitle1: {
        fontSize: 20
      },
      body1: {
        fontWeight: 900
      },
      button: {
        fontStyle: 'regular',
        disableFocusRipple: false,
        borderRadius: 10,
        border: 5
      }
    }
});

function App() {


  return (
    <MuiThemeProvider theme={theme}>
        <Container
              className="d-flex align-items-center justify-content-center"
              style={{ minHeight: "100vh" }}
        >
          <div className="w-100" style={{ maxWidth: "400px" }}>
            <Router>
              <AuthProvider>
                <Switch>
                  <PrivateRoute exact path="/" component={Dashboard}  />
                  <PrivateRoute exact path={"/classes/:classID"} component={ClassPage}/>
                  <PrivateRoute exact path={"/classes/:classID/assignments"} component={AssignmentsPage}/>
                  <PrivateRoute exact path={"/classes/:classID/classlist"} component={ClassList}/>
                  <PrivateRoute exact path={"/calendar"} component={CalendarPage}/>
                  <PrivateRoute exact path={"/messages"} component={MessagePage}/>
                  <PrivateRoute exact path={"/messages/:channelID"} component={ConversationPage}/>
                  <Route path="/login" component={Login} />
                </Switch>
              </AuthProvider>
            </Router>
          </div>
        </Container>
    </MuiThemeProvider>
    
  );
}

export default App;
